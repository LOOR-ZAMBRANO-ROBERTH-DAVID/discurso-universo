create table cliente(
	cliente_id varchar(4) primary key not null,
	cliente_cedula varchar(10) not null,
	cliente_nombre varchar(20) not null,
	cliente_apellido varchar(20) not null,
	cliente_telefono varchar(10),
	cliente_fecha_nacimiento date,
	cliente_estado_civil varchar(10),
	cliente_genero varchar(10),
	cliente_nacionalidad varchar(20),
	cliente_direccion varchar(50) not null,
	cliente_numero_hijos numeric
);
--Ingresamos clientes (10)
--delete from cliente
--select *from cliente
insert into cliente values ('C001', '1351355306', 'Roberth', 'Loor', '0923486291', '1999-10-10', 'Soltero', 'Masculino', 'ecuatoriana', 'calle 20 av 23', 2);
insert into cliente values ('C002', '1351359832', 'David', 'Zambrano', '0928154027', '1997-11-01', 'Casado', 'Masculino', 'ecuatoriana', 'calle 19 av 10', 1);
insert into cliente values ('C003', '1351357350', 'Carlos', 'Gomez', '0912348716', '1978-09-10', 'Soltero', 'Masculino', 'ecuatoriana', 'calle 13 av 12', 3);
insert into cliente values ('C004', '1351352444', 'Lucia', 'Zambrano', '0912666636', '1980-10-10', 'Casada', 'Femenino', 'ecuatoriana', 'calle 07 av 05', 5);
insert into cliente values ('C005', '1351351331', 'Hector', 'Garcia', '0911223399', '1970-02-02', 'Soltero', 'Masculino', 'ecuatoriana', 'calle 07 av 23', 1);
insert into cliente values ('C006', '1351353034', 'Rosaura', 'Zambrano', '0938164409', '1999-10-10', 'Casada', 'Femenino', 'ecuatoriana', 'calle 05 av 15', 1);
insert into cliente values ('C007', '1351351009', 'Estefania', 'Loor', '0928881308', '1997-12-11', 'Casada', 'Femenino', 'ecuatoriana', 'calle 08 av 08', 3);

create table hijo(
	hijo_id varchar(4) primary key not null,
	hijo_cedula varchar(10) not null,
	hijo_nombre varchar(20) not null,
	hijo_apellido varchar(20) not null,
	hijo_genero varchar (10),
	hijo_fecha_de_nacimiento date,
	hijo_nacionalidad varchar(20),
	hijo_disgusto varchar (50),
	
	hijo_cliente_id varchar(4) not null,
	constraint cliente_fk foreign key (hijo_cliente_id) references cliente (cliente_id)	
);
--Hijos Roberth
--delete from hijo
--select *from hijo
insert into hijo values ('H001', '1351354310', 'Mireya', 'Loor', 'Femenino', '2012-02-02', 'ecuatoriana', 'La piña', 'C001');
insert into hijo values ('H002', '1351352263', 'Oihane', 'Loor', 'Femenino', '2012-06-03', 'ecuatoriana', null, 'C001');
--Hijos David
insert into hijo values ('H003', '1351356620', 'Jon', 'Zambrano', 'Masculino', '2015-02-02', 'ecuatoriana', null, 'C002');
--Hijos Carlos
insert into hijo values ('H004', '1351352162', 'Antoni', 'Gomez', 'Masculino', '2013-03-01', 'ecuatoriana', null, 'C003');
insert into hijo values ('H005', '1351351811', 'Maria', 'Gomez', 'Femenino', '2017-12-11', 'ecuatoriana', null, 'C003');
insert into hijo values ('H006', '1351357398', 'Christopher', 'Gomez', 'Masculino', '2016-12-12', 'ecuatoriana', 'El Higado', 'C003');
--Hijos Lucia
insert into hijo values ('H007', '1351355378', 'Estela', 'Zambrano', 'Femenino', '2016-12-12', 'Venezona', 'El Higado', 'C004');
insert into hijo values ('H008', '1351356368', 'Christopher', 'Zambrano', 'Masculino', '2016-07-02', 'ecuatoriana', null, 'C004');
insert into hijo values ('H009', '1351357972', 'José', 'Zambrano', 'Masculino', '2015-06-13', 'ecuatoriana', null, 'C004');
insert into hijo values ('H010', '1351352914', 'Cynthia', 'Zambrano', 'Femenino', '2017-02-12', 'ecuatoriana', null, 'C004');
insert into hijo values ('H011', '1351356375', 'Patricio', 'Zambrano', 'Masculino', '2014-03-11', 'ecuatoriana', null, 'C004');
--Hijos Hector
insert into hijo values ('H012', '1351351085', 'David', 'Garcia', 'Masculino', '2007-11-10', 'ecuatoriana', null, 'C005');
--Rosaura
insert into hijo values ('H013', '1351357315', 'Mohamed', 'Zambrano', 'Masculino', '2014-03-11', 'ecuatoriana', 'La badea', 'C006');
--Hijos estefania
insert into hijo values ('H014', '1351354579', 'Cecilia', 'Loor', 'Femenino', '2013-02-04', 'ecuatoriana', null, 'C007');


create table nanny(
	nanny_id varchar(4) primary key not null,
	nanny_cedula varchar(10) not null,
	nanny_nombre varchar(20) not null,
	nanny_apellido varchar(20) not null,
	nanny_estado_civil varchar(10),
	nanny_nacionalidad varchar (11),
	nanny_ocupacion varchar (50),
	nanny_numero_hijos numeric,
	nanny_tiempo_laborando varchar(20),
	nanny_carrera_universitaria varchar(50)
	
);
--Se crean las niñeras
--select *from nanny
insert into nanny values ('NNN1', '1351358178', 'Rosalia', 'Jimenez', 'Soltera', 'ecuatoriana', 'Ama de casa', 0, '2 meses', null);
insert into nanny values ('NNN3', '1351355286', 'Lara', 'Baños', 'Casada', 'ecuatoriana', 'Estudiante', 1, '6 meses', 'Tecnologias en la Informacion');
insert into nanny values ('NNN4', '1351355464', 'Thais', 'Gonzales', 'Casada', 'ecuatoriana', 'Estudiante', 0, '11 meses', 'Auditoria');
insert into nanny values ('NNN5', '1351356627', 'Paz', 'Piqueras', 'Casada', 'ecuatoriana', 'Ama de casa', 0, '3 meses', null);

create table servicio(
	servicio_id varchar(4),
	sevricio_pago_realizado float,
	servicio_cant_hijos numeric,
	servicio_horas numeric,
	servicio_fecha date,
	servicio_inconvenientes varchar(50),
	servicio_calificacion numeric,
	
	servicio_cliente_id varchar(4),
	servicio_nanny_id varchar(4),
	constraint cliente_fk foreign key (servicio_cliente_id) references cliente(cliente_id),
	constraint nanny_fk foreign key (servicio_nanny_id) references nanny(nanny_id)
	
);
--Servicios realizador por la niñera Rosalia                 calificacion
--select *from servicio
--delete from servicio
insert into servicio values ('97MA', 8.00, 1, 1, '2020-12-03', null, 4, 'C001', 'NNN1');
insert into servicio values ('WG2U', 16.00, 2, 2, '2020-12-02', 'Percanse con tijeras', 2, 'C001', 'NNN1');
insert into servicio values ('NMS7', 8.000, 1, 1, '2020-11-01', null, 2, 'C002', 'NNN1');
insert into servicio values ('MY2B', 24.00, 3, 3, '2020-12-04', 'El niño se cayo de la cuna', 1, 'C003', 'NNN1');

--Servicios realizador por la niñera Lara                 calificacion
insert into servicio values ('9VKL', 13.00, 5, 2, '2020-10-03', null, 5, 'C004', 'NNN3');
insert into servicio values ('UK67', 16.00, 4, 3, '2020-12-03', null, 4, 'C004', 'NNN3');
insert into servicio values ('2EDU', 16.00, 5, 2, '2020-11-28', null, 5, 'C004', 'NNN3');
insert into servicio values ('7X5W', 16.00, 1, 2, '2020-11-17', null, 5, 'C005', 'NNN3');
insert into servicio values ('ULFG', 16.00, 1, 2, '2020-11-23', null, 5, 'C005', 'NNN3');

--Servicios realizador por la niñera Thais                 calificacion
insert into servicio values ('36XV', 24.00, 1, 4, '2020-11-23', null, 5, 'C006', 'NNN4');
insert into servicio values ('3W9M', 32.00, 3, 6, '2020-11-20', null, 4, 'C007', 'NNN4');
--Servicios realizados por la niñera Paz
insert into servicio values ('KB8Y', 20.00, 2, 3, '2020-11-19', null, 5, 'C001', 'NNN5');
insert into servicio values ('CY75', 15.00, 2, 2, '2020-12-03', null, 3, 'C001', 'NNN5');
insert into servicio values ('723X', 25.00, 1, 4, '2020-11-01', null, 5, 'C002', 'NNN5');
insert into servicio values ('S3QD', 30.00, 1, 5, '2020-11-02', null, 5, 'C002', 'NNN5');
insert into servicio values ('QEBN', 50.00, 3, 8, '2020-11-01', 'Los niños no estaban limpios', 3, 'C004', 'NNN5');
insert into servicio values ('T9KV', 10.00, 3, 1, '2020-10-20', null, 5, 'C004', 'NNN5');
insert into servicio values ('43LC', 8.00, 3, 1, '2020-11-15', null, 4, 'C007', 'NNN5');


--Se crean las alergias
create table alergia(
	alergia_id varchar (5) primary key not null,
	alergia_nombre varchar (20) not null
);

insert into alergia values('FKAPT', 'RinitisAlergica');
insert into alergia values('GUDCH', 'Urticaria');
insert into alergia values('DFGHJ', 'Polen');
insert into alergia values('KJHGF', 'Mascotas');
--select *from alergia



create table alergia_hijo(
	alergia_hijo_id varchar(5),
	
	alergia_hijo_alergia_id varchar (5) not null,
	alergia_hijo_hijo_id varchar (10),
	constraint alergia_hijo_fk foreign key (alergia_hijo_hijo_id) references hijo(hijo_id),
	constraint alergia_alergia_fk foreign key (alergia_hijo_alergia_id) references alergia(alergia_id)
);

insert into alergia_hijo values('1Q2W3', 'FKAPT', 'H001');
insert into alergia_hijo values('KKKKK', 'FKAPT', 'H002');
insert into alergia_hijo values('FHDJ3', 'GUDCH', 'H003');
insert into alergia_hijo values('LOLOJ', 'KJHGF', 'H003');
insert into alergia_hijo values('WGIT5', 'DFGHJ', 'H013');
insert into alergia_hijo values('VHUE6', 'KJHGF', 'H007');

--delete from alergia_hijo

create table sintoma_alergia(
	sintoma_alergia_id varchar (3) primary key not null,
	sintoma_alergia_alergia_id varchar (5),
	sintoma_alergia_descripcion varchar (50),
	
	constraint alergia_fk foreign key (sintoma_alergia_alergia_id) references alergia(alergia_id)
);

--Sintomas Rinitis alergica
insert into sintoma_alergia values ('QQQ', 'FKAPT', 'Goteo nasal');
insert into sintoma_alergia values ('AAA', 'FKAPT', 'Ojos llorosos');
insert into sintoma_alergia values ('ZZZ', 'FKAPT', 'Picazón en la nariz');
--Sintomas Urticaria
insert into sintoma_alergia values ('MMM', 'GUDCH', 'Ronchas rojas');
insert into sintoma_alergia values ('LLL', 'GUDCH', 'Picazón');
--Sintomas
insert into sintoma_alergia values ('POO', 'DFGHJ', 'Conjuntivitis');
insert into sintoma_alergia values ('AWD', 'DFGHJ', 'Picor en nariz');
insert into sintoma_alergia values ('RRR', 'DFGHJ', 'Congestión nasal');
insert into sintoma_alergia values ('TTT', 'DFGHJ', 'Dificultad para respirar');
--Sintomas de alergia a las mascotas
insert into sintoma_alergia values ('SDS', 'KJHGF', 'Estornudos');
insert into sintoma_alergia values ('III', 'KJHGF', 'resfrío');
insert into sintoma_alergia values ('UUU', 'KJHGF', 'Ojos rojos');




create table tratamiento_alergia(
	tratamiento_alergia_id varchar(4) primary key not null,
	tratamiento_alergia_alergia_id varchar(5),
	tratamiento_alergia_descripcion varchar(200),
	
	constraint alergia_fk foreign key (tratamiento_alergia_alergia_id) references alergia(alergia_id)
);

insert into tratamiento_alergia values ('RINI', 'FKAPT', 'loratadina, cetirizina y fexofenadina al percibir la alergia');
insert into tratamiento_alergia values ('URTI', 'GUDCH', 'Un antihistamínico oral de venta libre, como loratadina, cetirizina o difenhidramina');
insert into tratamiento_alergia values ('POLE', 'DFGHJ', 'Antihistamínicos: Difenhidramina (tiende a producir somnolencia)');
insert into tratamiento_alergia values ('MASC', 'KJHGF', 'Corticoesteroides en aerosol nasal como la fluticasona propionato, mometasona furoato, triamcinolona y ciclesonida');



-------------------------------CONSULTAS
--MOSTRAR TODAS LAS NIÑERAS CON SU NUMERO DE SERVICIOS PRESTADOS
/*
select 
(nanny.nanny_nombre || ' ' || nanny.nanny_apellido) AS Niñera,
count(servicio_nanny_id) as Numero_De_Servicios

from cliente inner join servicio on cliente_id=servicio_cliente_id
inner join nanny on nanny_id=servicio_nanny_id


group by nanny.nanny_nombre, nanny.nanny_apellido
order by numero_de_servicios desc
*/


--CONSULTA DEL DINERO QUE CADA NIÑERA OBTUVO EN EL ULTIMO MES
--Los servicios realizados fuera del mes no se contaran
/*
select
(nanny.nanny_nombre || ' ' || nanny.nanny_apellido) as Niñera,
sum(servicio.sevricio_pago_realizado) as total

from cliente inner join servicio on cliente_id=servicio_cliente_id
inner join nanny on nanny_id=servicio_nanny_id

where current_date - servicio.servicio_fecha <=30
group by nanny.nanny_nombre,nanny.nanny_apellido
order by total desc

*/

--MOSTRAR EN ORDEN DESENDENTE LOS CLIENTES Y CUANTO HAN APORTADO
--EN EL NEGOCIO DE NIÑERAS A DOMICILIO
/*

select 
(cliente.cliente_nombre || ' ' || cliente.cliente_apellido) as Cliente,
sum(sevricio_pago_realizado) as total_dolares,
count(servicio.servicio_id) as numero_de_servicios

from cliente inner join servicio on cliente_id=servicio_cliente_id
inner join nanny on nanny_id=servicio_nanny_id

group by cliente.cliente_nombre, cliente.cliente_apellido
order by total_dolares desc

*/

--CONSULTA DE LAS CALIFICACIONES DE LAS NINIERAS
/*
select  
(nanny.nanny_nombre || ' ' || nanny.nanny_apellido) as Niñera,
round(avg(servicio.servicio_calificacion), 2) as calificacion


from cliente inner join servicio on cliente_id=servicio_cliente_id
inner join nanny on nanny_id=servicio_nanny_id
group by nanny.nanny_nombre, nanny.nanny_apellido
order by round(avg(servicio.servicio_calificacion), 2) desc

*/

/*
-----MOSTRAR CADA HIJO JUNTO A SU ALERGIA Y TRATAMIENTO
select
(hijo.hijo_nombre || ' ' || hijo.hijo_apellido) as hijo,
(cliente.cliente_nombre || ' ' || cliente.cliente_apellido) as padre_de_familia,
cliente.cliente_telefono as telefono,
alergia.alergia_nombre as alergia,
tratamiento_alergia.tratamiento_alergia_descripcion as tratamiento

from cliente inner join hijo on hijo_cliente_id = cliente.cliente_id
inner join alergia_hijo on hijo.hijo_id = alergia_hijo.alergia_hijo_hijo_id
inner join alergia on alergia.alergia_id = alergia_hijo.alergia_hijo_alergia_id
inner join tratamiento_alergia on tratamiento_alergia_alergia_id = alergia.alergia_id
order by cliente.cliente_nombre
*/
