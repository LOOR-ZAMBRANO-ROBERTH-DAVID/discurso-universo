
-------------------------------CONSULTAS
--MOSTRAR TODAS LAS NIÑERAS CON SU NUMERO DE SERVICIOS PRESTADOS

select 
(nanny.nanny_nombre || ' ' || nanny.nanny_apellido) AS Niñera,
count(servicio_nanny_id) as Numero_De_Servicios

from cliente inner join servicio on cliente_id=servicio_cliente_id
inner join nanny on nanny_id=servicio_nanny_id


group by nanny.nanny_nombre, nanny.nanny_apellido
order by numero_de_servicios desc



--CONSULTA DEL DINERO QUE CADA NIÑERA OBTUVO EN EL ULTIMO MES
--Los servicios realizados fuera del mes no se contaran

select
(nanny.nanny_nombre || ' ' || nanny.nanny_apellido) as Niñera,
sum(servicio.sevricio_pago_realizado) as total

from cliente inner join servicio on cliente_id=servicio_cliente_id
inner join nanny on nanny_id=servicio_nanny_id

where current_date - servicio.servicio_fecha <=30
group by nanny.nanny_nombre,nanny.nanny_apellido
order by total desc



--MOSTRAR EN ORDEN DESENDENTE LOS CLIENTES Y CUANTO HAN APORTADO
--EN EL NEGOCIO DE NIÑERAS A DOMICILIO


select 
(cliente.cliente_nombre || ' ' || cliente.cliente_apellido) as Cliente,
sum(sevricio_pago_realizado) as total_dolares,
count(servicio.servicio_id) as numero_de_servicios

from cliente inner join servicio on cliente_id=servicio_cliente_id
inner join nanny on nanny_id=servicio_nanny_id

group by cliente.cliente_nombre, cliente.cliente_apellido
order by total_dolares desc



--CONSULTA DE LAS CALIFICACIONES DE LAS NINIERAS

select  
(nanny.nanny_nombre || ' ' || nanny.nanny_apellido) as Niñera,
round(avg(servicio.servicio_calificacion), 2) as calificacion


from cliente inner join servicio on cliente_id=servicio_cliente_id
inner join nanny on nanny_id=servicio_nanny_id
group by nanny.nanny_nombre, nanny.nanny_apellido
order by round(avg(servicio.servicio_calificacion), 2) desc




-----MOSTRAR CADA HIJO JUNTO A SU ALERGIA Y TRATAMIENTO
select
(hijo.hijo_nombre || ' ' || hijo.hijo_apellido) as hijo,
(cliente.cliente_nombre || ' ' || cliente.cliente_apellido) as padre_de_familia,
cliente.cliente_telefono as telefono,
alergia.alergia_nombre as alergia,
tratamiento_alergia.tratamiento_alergia_descripcion as tratamiento

from cliente inner join hijo on hijo_cliente_id = cliente.cliente_id
inner join alergia_hijo on hijo.hijo_id = alergia_hijo.alergia_hijo_hijo_id
inner join alergia on alergia.alergia_id = alergia_hijo.alergia_hijo_alergia_id
inner join tratamiento_alergia on tratamiento_alergia_alergia_id = alergia.alergia_id
order by cliente.cliente_nombre

